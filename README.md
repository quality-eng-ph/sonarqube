# Continuous Code Quality

![SonarQube logo](https://upload.wikimedia.org/wikipedia/commons/e/e6/Sonarqube-48x200.png)

by: Tems Canaveral

## Prerequisites
  - basic linux knowledge
  - [docker](https://www.docker.com) (_Note:_ should include `docker-machine` and `docker-compose`)
  - [gitlab](https://about.gitlab.com)
  - [jenkins](https://jenkins.io/index.html)

## Installation: Option A - `docker`
Install [sonarqube](https://www.sonarqube.org) from [docker store](https://store.docker.com/images/3f8fc4ce-eb8e-40ad-88ba-69e97299c64f?tab=description)

~~~bash
$ docker pull sonarqube
~~~

Install [postgres](https://www.postgresql.org/) from [docker store](https://store.docker.com/images/022689bf-dfd8-408f-9e1c-19acac32e57b)

~~~bash
$ docker pull postgres
~~~

Setup a postgres schema for sonarqube:

~~~bash
$ docker run -d --name sonardb \
    -e POSTGRESQL_USER=sonar \
    -e POSTGRESQL_PASS=sonar \
    -e POSTGRESQL_DB=sonar \
    postgres
~~~

Run sonarqube, linking its data storage to sonar schema:

~~~bash
$ docker run -d --name sonarqube \
    --link sonardb:db \
    -p 9000:9000 \
    -e SONARQUBE_JDBC_URL=jdbc:postgresql://sonardb:5432/sonar \
    sonarqube
~~~

## Installation: Option B - `docker-compose`
Create a new directory where you can store your `docker-compose.yml` file:

~~~bash
$ mkdir newDir
~~~

Switch to this new directory

~~~bash
$ cd newDir
~~~

Create `docker-compose.yml` file using:

~~~bash
$ vi docker-compse.yml
~~~

Then paste this code:

~~~docker
version: "2"

services:
  sonarqube:
    image: sonarqube
    ports:
      - "9000:9000"
    networks:
      - sonarnet
    environment:
      - SONARQUBE_JDBC_URL=jdbc:postgresql://sonarqube-postgres:5432/sonar
    volumes:
      - sonarqube_conf:/opt/sonarqube/conf
      - sonarqube_data:/opt/sonarqube/data
      - sonarqube_extensions:/opt/sonarqube/extensions
      - sonarqube_bundled-plugins:/opt/sonarqube/lib/bundled-plugins

  sonarqube-postgres:
    image: postgres
    networks:
      - sonarnet
    environment:
      - POSTGRES_USER=sonar
      - POSTGRES_PASSWORD=sonar
    volumes:
      - postgresql:/var/lib/postgresql
      - postgresql_data:/var/lib/postgresql/data

networks:
  sonarnet:
    driver: bridge

volumes:
  sonarqube_conf:
  sonarqube_data:
  sonarqube_extensions:
  sonarqube_bundled-plugins:
  postgresql:
  postgresql_data:
~~~

Save file by pressing `Esc` button then typing `:wq`.

Run application by running:

~~~bash
$ docker-compose up &
~~~

###Checking for docker logs

Check logs via

~~~bash
$ docker logs -f <image-name>
~~~

###Analyzing Code

For Java (using gradle), setup gradle.properties:

~~~java
systemProp.sonar.host.url=<host>:<sonarqube-port>
systemProp.sonar.login=<sonarqube-user-token>
~~~

Then in you java project, create profile for sonarqube like this one:

~~~gradle
sonarqube {
  properties {
    property "sonar.projectName", "Java :: Simple Project :: SonarQube Scanner for Gradle"
    property "sonar.projectKey", "org.sonarqube:java-gradle-simple"
    property "sonar.jacoco.reportPaths", "${project.buildDir}/jacoco/test.exec"
  }
}
~~~

Run sonar scans via:

~~~bash
$ gradle sonarqube
~~~

If your scan succeeds, you can already check the sonarqube results in your server.
